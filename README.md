You need to install docker and docker compose before start

Docker Quickstart

- docker-compose up -d

Stop containers

 - docker-compose stop

Start containers

 - docker-compose start

start with console

 - docker-compose run --rm app bash

Web aceess Url:
---------------
 http://localhost:8000
 
=================================

For MySQL access
--------------------
You can choose between MySQL (default), MariaDB and PerconaDB in docker/mysql/Dockerfile

Setting	Value
User	     :  dev (if not changed in env)
Password     : 	dev (if not changed in env)
Database     :	database (if not changed in env)
Host	     :  mysql:3306
External Port:	13306
Access fo MySQL user "root" and "dev" will be allowed from external hosts (eg. for debugging, dumps and other stuff).
